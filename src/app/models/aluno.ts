export interface Aluno {
    id: number;
    nome: string;
    email: string;
    idade: number;
    formacao: string;
    cursos?: [];
    senha: string;
    tipo: number;

}
